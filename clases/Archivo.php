<?php
/**
 * Created by PhpStorm.
 * User: Mauricio
 * Date: 21/10/2019
 * Time: 08:59
 */

namespace Migracion;


class Archivo
{
    private $manejador;//Atributo de manejador de Archivos

    //Constructor se la pasa el path del archivo1 y el modo de lectura (opcional)
    public function __construct( $path, $modo = "r" )
    {
        $this->manejador = fopen($path,$modo) or die("No se pudo abrir el archivo1");
    }

    //Método que lee lénea a línea el archivo. El archivo debe tener el siguiente formado:  texto=texto (en cada línea)
    //Cada línea representa la siguiente informaciín parametroConfigBD=valorParametro (en cada línea)
    public function obtenerConfiguracion()
    {
        $response = array();//Array asociativo de parámetros de conexión

        //Ciclo que recorre el archivo hasta el final
        while ( !feof($this->manejador) )
        {
            $line               = fgets($this->manejador);//Lee el archivo línea a línea
            list($index,$value) = explode("=",$line);//Divide la cadena separada por signo = y guarda en las variables $index y $value
            $response[$index]   = trim($value);//Crea un vector asociativo (donde el index es el parámetro de configuración de la BD y $value es el valor
        }

        return $response;//Devuelve el array asociativo
    }

    //El destructor libera el recurso de archivo1
    public function __destruct()
    {
        fclose($this->manejador);
    }
}