<?php
/**
 * Created by PhpStorm.
 * User: Mauricio
 * Date: 21/10/2019
 * Time: 08:58
 */

namespace Migracion;


class Db
{
    private $conn;//Recurso de conexión a Base de Datos.

    //El constructor del objeto, toma los parámetros de configuración de conexión desde el archivo1 de configuración.
    //Se toma por defecto una ubicación del archivo1 de configuración.
    public function __construct( $path = "config/configbd.inc" )
    {
        date_default_timezone_set('America/Asuncion');

        //Creación del objeto FILE para determinar los parámetros de conexión que se encuentran configurados en el archivo1 de configuración
        $file   = new Archivo($path);
        $config = $file->obtenerConfiguracion();
        $dbname = $config["dbname"];
        $host   = $config["host"];
        $user   = $config["user"];
        $pass   = $config["pass"];
        $port   = $config["port"];

        //Se realiza la conexión utilizando PDO
        try {

            //Se crea el objeto PDO y se le pasa al constructor el DSN (formado a partir de los parámetros de conexión obtenidos del archivo1 de configuracion).
            $this->conn = new \PDO("pgsql:host=$host;port=$port;dbname=$dbname;user=$user;password=$pass");
            $this->conn->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);//Se especifica para que se lancen excepciones

        }catch( PDOException $e ) {//En el caso de error se tira la excepción, se captura y genera la respuesta json estandarizada

            $erroresArray[] = "Error de Conexión a Base de Datos: " . utf8_encode($e->getMessage());
            $this->respuestaJson->crearRespuestaJson(false, array(), $erroresArray);
            die();
        }
    }

    //Método destructor: Para cerrar la conexión, es necesario destruir el objeto asegurándose de que todas las referencias a él existentes
    //sean eliminadas
    public function __destruct()
    {
        $this->conn = null;
    }

    //Método que ejecuta un query de tipo select
        public function ejecutarSelect($query)
    {
        //Prepara el query
        $queryPre = $this->conn->prepare($query);

        if(!$queryPre->execute())
            return $query;//Si existe error en la ejecución del query, devuelve el query
        else{
            //Si no existe error devuelve un array asociativo con los registros respectivos
            $this->numRowAffected = $queryPre->rowCount();//Determina la cantidad de registros afectados
            return $queryPre->fetchAll(\PDO::FETCH_ASSOC);
        }
    }

    //Método que ejecuta un query de tipo select
    public function guardarInsertMasivos($sql)
    {
        $this->conn->beginTransaction();

        try {

            $this->conn->exec($sql);
            $this->conn->commit();

        }catch (\PDOException $e){
            //Rollback the transaction.
            $this->conn->rollBack();
            echo $e->getMessage();
            die('Error en la transacción');
        }
    }
}