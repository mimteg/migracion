<?php
/**
 * Created by PhpStorm.
 * User: Mauricio
 * Date: 21/10/2019
 * Time: 00:00
 */
require_once 'vendor/autoload.php';

use Migracion\Db;

$conn = new Db();

$reader = \PhpOffice\PhpSpreadsheet\IOFactory::createReader('Xlsx');
$reader->setReadDataOnly(TRUE);
$spreadsheet   = $reader->load("migracion.xlsx");
$worksheet     = $spreadsheet->getActiveSheet();
// Get the highest row and column numbers referenced in the worksheet
$highestRow    = $worksheet->getHighestRow(); // e.g. 10
$highestColumn = $worksheet->getHighestColumn(); // e.g 'F'
$highestColumnIndex = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::columnIndexFromString($highestColumn); // e.g. 5

echo '<table border="1">' . "\n";
for ($row = 1; $row <= $highestRow; ++$row) {
    echo '<tr>' . PHP_EOL;
    for ($col = 1; $col <= $highestColumnIndex; ++$col) {
        $value = $worksheet->getCellByColumnAndRow($col, $row)->getValue();
        echo '<td>' . $value . '</td>' . PHP_EOL;
    }
    echo '</tr>' . PHP_EOL;
}
echo '</table>' . PHP_EOL;

$sql="";

for ($row = 1; $row <= $highestRow; ++$row) {

    $sql.="insert into producto (tipo_id,marca_id,nombre,descripcion) values(";
    for ($col = 1; $col <= $highestColumnIndex; ++$col) {

        if($col==1||$col==2)
            $sql.=$worksheet->getCellByColumnAndRow($col, $row)->getValue().",";
        elseif($col==3)
            $sql.="'".$worksheet->getCellByColumnAndRow($col, $row)->getValue()."',";
        else
            $sql.="'".$worksheet->getCellByColumnAndRow($col, $row)->getValue()."')";
    }
    $sql.=";";
}

echo $sql;

$conn->guardarInsertMasivos($sql);